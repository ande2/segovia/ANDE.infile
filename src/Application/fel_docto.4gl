IMPORT XML

SCHEMA segovia

GLOBALS "fel_globales.4gl"
GLOBALS "../Library/ws_g4s/ws_g4s_fel.inc"
GLOBALS "../Library/sat/sat_xml_docto.inc"
GLOBALS "../Library/sat/sat_xml_anula.inc"
GLOBALS "../Library/sat/sat_xml_fcam.inc"

FUNCTION fel_docto_build()
DEFINE nit            VARCHAR(25)
DEFINE frases         RECORD LIKE empresas1_f.*
DEFINE f              SMALLINT
DEFINE numero_acceso  DECIMAL(9,0)
DEFINE adenda         SMALLINT
DEFINE mensaje    STRING
DEFINE resultado  SMALLINT

    CALL STARTLOG("ERR_ANDE")

    LET adenda = FALSE
    INITIALIZE ns1GTDocumento.* TO NULL 

    LET fel.fac_id = id    
    LET fel.request_id = id USING "&&&&&&&&&&"
    LET fel.fecha_envio = CURRENT
    LET fel.correlativo = ultimo_corr_msg()
    LET fel.numero_acceso = numero_acceso
    LET fel.estatus = 1 -- ESTADO INICIAL
    LET fel.interna_tipod  = factura.tipod
    LET fel.interna_serie  = factura.serie
    LET fel.interna_numero = factura.num_doc
    LET fel.flag_error = 0

    INSERT INTO factura_log VALUES (fel.*)

    TRY
        IF fel.estatus > 0 THEN
            ## -- DATOS BASICOS
            LET ns1GTDocumento.Version = 0.1
            LET ns1GTDocumento.SAT.ClaseDocumento = 'dte'
            LET ns1GTDocumento.SAT.DTE.ID = 'DatosCertificados'
            LET ns1GTDocumento.SAT.DTE.DatosEmision.ID = 'DatosEmision'

            # Datos Emision
            LET ns1GTDocumento.SAT.DTE.DatosEmision.DatosGenerales.CodigoMoneda = factura.c_moneda CLIPPED

            # Exportación
            IF factura.es_exportacion = 1 THEN
               LET ns1GTDocumento.SAT.DTE.DatosEmision.DatosGenerales.Exp = "SI"
               LET ns1GTDocumento.SAT.DTE.DatosEmision.Receptor.TipoEspecial = 'EXT'
            END IF 
            
            ## --DATOS GENERALES
            LET ns1GTDocumento.SAT.DTE.DatosEmision.DatosGenerales.FechaHoraEmision = factura.fecha_em CLIPPED --FHEmision
            #Tipo
            LET ns1GTDocumento.SAT.DTE.DatosEmision.DatosGenerales.Tipo = factura.tipo_doc CLIPPED
            --LET ns1GTDocumento.SAT.DTE.DatosEmision.DatosGenerales.NumeroAcceso = numero_acceso

            ## DATOS DE EMISOR
            LET ns1GTDocumento.SAT.DTE.DatosEmision.Emisor.AfiliacionIVA                = 'GEN'
            --Establecimiento
            SELECT sat_establecimiento INTO conexion.cod_sucursal
               FROM fac_puntovta WHERE numpos = factura.sat_establecimiento
            IF conexion.cod_sucursal IS NULL OR conexion.cod_sucursal <= 0 THEN
               LET conexion.cod_sucursal = 1
            END IF 
            LET ns1GTDocumento.SAT.DTE.DatosEmision.Emisor.CodigoEstablecimiento        = conexion.cod_sucursal CLIPPED
            LET ns1GTDocumento.SAT.DTE.DatosEmision.Emisor.CorreoEmisor                 = factura.from CLIPPED
            LET ns1GTDocumento.SAT.DTE.DatosEmision.Emisor.NITEmisor                    = utl_nit_singuion(conexion.nit)
            LET ns1GTDocumento.SAT.DTE.DatosEmision.Emisor.NombreComercial              = factura.nombre_c CLIPPED
            LET ns1GTDocumento.SAT.DTE.DatosEmision.Emisor.NombreEmisor                 = factura.nombre_e CLIPPED
            
            LET ns1GTDocumento.SAT.DTE.DatosEmision.Emisor.DireccionEmisor.Direccion    = factura.direccion_e CLIPPED, ' ', conexion.direccion2
            LET ns1GTDocumento.SAT.DTE.DatosEmision.Emisor.DireccionEmisor.CodigoPostal = factura.codpos_e CLIPPED
            LET ns1GTDocumento.SAT.DTE.DatosEmision.Emisor.DireccionEmisor.Municipio    = factura.municipio_e CLIPPED
            LET ns1GTDocumento.SAT.DTE.DatosEmision.Emisor.DireccionEmisor.Departamento = factura.departamento_e CLIPPED
            LET ns1GTDocumento.SAT.DTE.DatosEmision.Emisor.DireccionEmisor.Pais         = 'GT'

            # Datos Receptor
            LET ns1GTDocumento.SAT.DTE.DatosEmision.Receptor.CorreoReceptor = factura.to CLIPPED

            --IF factura.es_exportacion = 1 THEN
            --   LET ns1GTDocumento.SAT.DTE.DatosEmision.Receptor.IDReceptor = factura.nit_r
            --ELSE    
             
               LET factura.nit_r = quitar_guion(factura.nit_r)
               LET nit = utl_nit_singuion(factura.nit_r)
               LET ns1GTDocumento.SAT.DTE.DatosEmision.Receptor.IDReceptor = factura.nit_r 
           -- END IF
            LET ns1GTDocumento.SAT.DTE.DatosEmision.Receptor.NombreReceptor = factura.nombre_r CLIPPED
            
            LET ns1GTDocumento.SAT.DTE.DatosEmision.Receptor.DireccionReceptor.Direccion = factura.direccion_r CLIPPED
            LET ns1GTDocumento.SAT.DTE.DatosEmision.Receptor.DireccionReceptor.Departamento = factura.departamento_r CLIPPED
            LET ns1GTDocumento.SAT.DTE.DatosEmision.Receptor.DireccionReceptor.Municipio    = factura.municipio_r CLIPPED
            
            IF factura.codpos_r IS NULL THEN
                LET factura.codpos_r = 100
            END IF
            LET ns1GTDocumento.SAT.DTE.DatosEmision.Receptor.DireccionReceptor.CodigoPostal = factura.codpos_r CLIPPED
            
            LET ns1GTDocumento.SAT.DTE.DatosEmision.Receptor.DireccionReceptor.Pais = factura.pais_r CLIPPED
            

            ## DETALLE FACTURA
            CALL fel_factura_items()

            LET fel.estatus = 2 --TERMINÓ DE GRABAR INFORMACIÓN BÁSICA
            
            ## ADENDA Y COMPLEMENTOS
            CASE factura.tipo_doc
                WHEN "FACT" 
                    IF factura.ES_CUI = 1 THEN
                        LET ns1GTDocumento.SAT.DTE.DatosEmision.Receptor.TipoEspecial = 'CUI'
                    ELSE 
                        IF factura.ES_CUI = 2 THEN
                           LET ns1GTDocumento.SAT.DTE.DatosEmision.Receptor.TipoEspecial = 'EXT'
                        END IF 
                    END IF
                    DECLARE cur_frasesfact CURSOR FOR
                        SELECT *
                          FROM empresas1_f
                    LET f = 0
                    FOREACH cur_frasesfact INTO frases.*
                         LET f = f + 1
                         LET ns1GTDocumento.SAT.DTE.DatosEmision.Frases.Frase[f].CodigoEscenario = frases.codigo_escenario
                         LET ns1GTDocumento.SAT.DTE.DatosEmision.Frases.Frase[f].TipoFrase = frases.tipo_frase
                    END FOREACH
                    DISPLAY "Factura exenta -> SELECT cod_frase, cod_escenario ",
                              " FROM  fac_clientes_fra, fac_mtransac ",
                              " WHERE fac_mtransac.lnktra = ", factura.num_int,
                              " AND   fac_mtransac.codcli = fac_clientes_fra.codcli "
                    IF factura.es_exenta = 1 THEN --factura exenta
                        DECLARE curfrasescli CURSOR FOR 
                           SELECT cod_frase, cod_escenario
                              FROM  fac_clientes_fra, fac_mtransac
                              WHERE fac_mtransac.lnktra = factura.num_int
                              AND   fac_mtransac.codcli = fac_clientes_fra.codcli
                        FOREACH curfrasescli INTO frases.*
                           LET f = f + 1
                           LET ns1GTDocumento.SAT.DTE.DatosEmision.Frases.Frase[f].CodigoEscenario = frases.codigo_escenario
                           LET ns1GTDocumento.SAT.DTE.DatosEmision.Frases.Frase[f].TipoFrase = frases.tipo_frase
                        END FOREACH
                        LET f = f + 1   
                    END IF 
                    CALL fel_adenada_fact()
                    IF factura.es_exportacion = 1 THEN
                        LET f = f + 1
                        LET ns1GTDocumento.SAT.DTE.DatosEmision.Frases.Frase[f].CodigoEscenario = 1
                        LET ns1GTDocumento.SAT.DTE.DatosEmision.Frases.Frase[f].TipoFrase = 4
                        
                        LET ns1GTDocumento.SAT.DTE.DatosEmision.Complementos._LIST_0[1].Complemento.NombreComplemento = 'Complemento_Exportacion'
                        LET ns1GTDocumento.SAT.DTE.DatosEmision.Complementos._LIST_0[1].Complemento.URIComplemento = 'http://www.sat.gob.gt/face2/ComplementoExportaciones/0.1.0'
                        LET ns1GTDocumento.SAT.DTE.DatosEmision.Complementos._LIST_0[1].Complemento._ANY_0 = fel_complento_FACT_EXP()
                     END IF 
                WHEN "FCAM" 
                    DECLARE cur_frases CURSOR FOR
                        SELECT *
                          FROM empresas1_f
                    LET f = 0
                    FOREACH cur_frases INTO frases.*
                         LET f = f + 1
                         LET ns1GTDocumento.SAT.DTE.DatosEmision.Frases.Frase[f].CodigoEscenario = frases.codigo_escenario
                         LET ns1GTDocumento.SAT.DTE.DatosEmision.Frases.Frase[f].TipoFrase = frases.tipo_frase
                    END FOREACH
                    CALL fel_adenada_fcam()
                    LET ns1GTDocumento.SAT.DTE.DatosEmision.Complementos._LIST_0[1].Complemento.NombreComplemento = 'FACTURA_CAMBIARIA'
                    LET ns1GTDocumento.SAT.DTE.DatosEmision.Complementos._LIST_0[1].Complemento.URIComplemento = 'http://www.sat.gob.gt/dte/fel/CompCambiaria/0.1.0'
                    LET ns1GTDocumento.SAT.DTE.DatosEmision.Complementos._LIST_0[1].Complemento._ANY_0 = fel_complento_FCAM()    
                WHEN "NCRE" 
                    CALL fel_adenada_nota()
                    LET ns1GTDocumento.SAT.DTE.DatosEmision.Complementos._LIST_0[1].Complemento.NombreComplemento = 'NOTA_CREDITO'
                    LET ns1GTDocumento.SAT.DTE.DatosEmision.Complementos._LIST_0[1].Complemento.URIComplemento = 'http://www.sat.gob.gt/face2/ComplementoReferenciaNota/0.1.0'
                    LET ns1GTDocumento.SAT.DTE.DatosEmision.Complementos._LIST_0[1].Complemento._ANY_0 = fel_complento_nota()
                WHEN "NDEB" 
                    CALL fel_adenada_nota()
                    LET ns1GTDocumento.SAT.DTE.DatosEmision.Complementos._LIST_0[1].Complemento.NombreComplemento = 'NOTA_DEBITO'
                    LET ns1GTDocumento.SAT.DTE.DatosEmision.Complementos._LIST_0[1].Complemento.URIComplemento = 'http://www.sat.gob.gt/face2/ComplementoReferenciaNota/0.1.0'
                    LET ns1GTDocumento.SAT.DTE.DatosEmision.Complementos._LIST_0[1].Complemento._ANY_0 = fel_complento_nota()
                WHEN "FESP" 
                    IF factura.ES_CUI = 1 THEN
                        LET ns1GTDocumento.SAT.DTE.DatosEmision.Receptor.TipoEspecial = 'CUI'
                    END IF 
                    LET ns1GTDocumento.SAT.DTE.DatosEmision.Complementos._LIST_0[1].Complemento.NombreComplemento = 'FACTURA_ESPECIAL'
                    LET ns1GTDocumento.SAT.DTE.DatosEmision.Complementos._LIST_0[1].Complemento.URIComplemento = 'http://www.sat.gob.gt/face2/ComplementoFacturaEspecial/0.1.0'
                    LET ns1GTDocumento.SAT.DTE.DatosEmision.Complementos._LIST_0[1].Complemento._ANY_0 = fel_complemento_FESP()
                    CALL fel_adenada_FESP()
            END CASE

            LET fel.estatus = 3 --TERMINA DE GRABAR ADENDAS Y COMPLEMENTOS
            CALL fel_arch_original()

        END IF
    CATCH
        LET mensaje = "FEL 2.0 (Archivo) ERROR :",STATUS||" ("||SQLCA.SQLERRM||")"
        LET resultado = FALSE
        LET fel.flag_error = 1
        LET fel.msg_error = mensaje
    END TRY

END FUNCTION


PRIVATE FUNCTION fel_factura_items()
DEFINE det      RECORD LIKE facturafel_ed.*
DEFINE i        SMALLINT
DEFINE TotalItem, ImpuestoItem DECIMAL(18,6)
DEFINE GranTotal, TotalImpuesto DECIMAL(18,6)
DEFINE mensaje    STRING
DEFINE resultado  SMALLINT

     CALL STARTLOG("ERR_ANDE")
     
    INITIALIZE det.* TO NULL
    LET i = 0
    LET GranTotal = 0
    LET TotalImpuesto = 0

   TRY
   DECLARE cur_detart CURSOR FOR
     SELECT *
       FROM facturafel_ed
       WHERE fac_id = factura.fac_id

   CALL detcods.CLEAR()
   FOREACH cur_detart INTO det.*
        LET i = i + 1
        LET TotalItem = 0
        LET ImpuestoItem = 0 

        LET detcods[i].codigo = det.codigo_p

        LET ns1GTDocumento.SAT.DTE.DatosEmision.Items.Item[i].BienOServicio  = det.categoria[1,1]
        LET ns1GTDocumento.SAT.DTE.DatosEmision.Items.Item[i].NumeroLinea    = i
        LET ns1GTDocumento.SAT.DTE.DatosEmision.Items.Item[i].Cantidad       = det.cantidad
        LET ns1GTDocumento.SAT.DTE.DatosEmision.Items.Item[i].UnidadMedida   = det.unidad_m
        LET ns1GTDocumento.SAT.DTE.DatosEmision.Items.Item[i].Descripcion    = det.descrip_p
        LET ns1GTDocumento.SAT.DTE.DatosEmision.Items.Item[i].PrecioUnitario = det.precio_u
        LET ns1GTDocumento.SAT.DTE.DatosEmision.Items.Item[i].Precio         = det.precio_t
        LET ns1GTDocumento.SAT.DTE.DatosEmision.Items.Item[i].Descuento      = det.descto_t
        LET ns1GTDocumento.SAT.DTE.DatosEmision.Items.Item[i].Total          = det.base + det.monto

        # Cuando la descripción es larga
        IF det.txt_especial = "S" THEN 
           LET ns1GTDocumento.SAT.DTE.DatosEmision.Items.Item[i].Descripcion = det.txt_largo
        END IF 

        LET TotalItem    =  det.base + det.monto
        LET ImpuestoItem = 0
        IF factura.tipo_doc <> 'NABN' THEN

            IF factura.es_exportacion = 1 OR factura.es_exenta = 1 THEN
               LET ns1GTDocumento.SAT.DTE.DatosEmision.Items.Item[i].Impuestos.Impuesto[1].CodigoUnidadGravable = 2
            ELSE 
               LET ns1GTDocumento.SAT.DTE.DatosEmision.Items.Item[i].Impuestos.Impuesto[1].CodigoUnidadGravable = 1
            END IF    
                
            LET ns1GTDocumento.SAT.DTE.DatosEmision.Items.Item[i].Impuestos.Impuesto[1].NombreCorto   = det.tipo
            LET ns1GTDocumento.SAT.DTE.DatosEmision.Items.Item[i].Impuestos.Impuesto[1].MontoGravable = det.base
            LET ns1GTDocumento.SAT.DTE.DatosEmision.Items.Item[i].Impuestos.Impuesto[1].MontoImpuesto = det.monto
        END IF

   END FOREACH
   CLOSE cur_detart

    IF factura.tipo_doc <> 'NABN' THEN
        LET ns1GTDocumento.SAT.DTE.DatosEmision.Totales.TotalImpuestos.TotalImpuesto[1].NombreCorto = 'IVA'
        LET ns1GTDocumento.SAT.DTE.DatosEmision.Totales.TotalImpuestos.TotalImpuesto[1].TotalMontoImpuesto = factura.monto1
    END IF
    LET ns1GTDocumento.SAT.DTE.DatosEmision.Totales.GranTotal = factura.total_neto
   CATCH
        LET mensaje = "FEL 2.0 (Archivo) ERROR :",STATUS||" ("||SQLCA.SQLERRM||")"
        LET resultado = FALSE
        LET fel.flag_error = 1
        LET fel.msg_error = mensaje
   END TRY
END FUNCTION

PRIVATE FUNCTION fel_arch_original()
DEFINE writertFEL xml.StaxWriter
DEFINE mensaje    STRING
DEFINE resultado  SMALLINT
DEFINE wsstatus   INTEGER
DEFINE retryAuth  INTEGER
DEFINE retryProxy INTEGER
DEFINE retry      INTEGER
DEFINE comando    STRING
DEFINE docXML     xml.DomDocument
DEFINE docXMLT    xml.DomDocument
DEFINE root       xml.DomNode
DEFINE archtmp    STRING
  CALL STARTLOG("ERR_ANDE")

  LET wsstatus = -1
  LET retryAuth = FALSE
  LET retryProxy = FALSE
  LET retry = TRUE
  LET fel.dir_arch_org = directorio2 CLIPPED,'/original/ori_', fel.request_id CLIPPED,'.xml'
  LET fel.dir_arch_fir = directorio2 CLIPPED,'/firmado/fir_', fel.request_id CLIPPED,'.xml'
  LET fel.dir_arch_cer = directorio CLIPPED,'/cer_', fel.request_id CLIPPED,'.xml'

   LET comando = "echo ", "dir_arch_original ", fel.dir_arch_org, ">> ande_cert.log"
   RUN comando 
   LET comando = "echo ", "dir_arch_fir ", fel.dir_arch_fir, ">> ande_cert.log"
   RUN comando 
   LET comando = "echo ", "dir_arch_cer ", fel.dir_arch_cer, ">> ande_cert.log"
   RUN comando 
   DISPLAY "dir_arch_original ", fel.dir_arch_org 
   DISPLAY "dir_arch_fir ", fel.dir_arch_fir 
   DISPLAY "dir_arch_cer ", fel.dir_arch_cer 
  
  LET resultado = FALSE
  TRY
      LET archtmp = fel_archivo_genera()
      LET writertFEL = xml.StaxWriter.Create()
      CALL writertFEL.setFeature("format-pretty-print",TRUE)
      CALL writertFEL.writeTo(archtmp) --fel.dir_arch_org)
      CALL writertFEL.startDocument("utf-8","1.0",FALSE)
      CALL xml.Serializer.VariableToStax(ns1GTDocumento,writertFEL)
      CALL writertFEL.endDocument()
      CALL writertFEL.CLOSE()
      
      LET docXMLT = xml.DomDocument.CREATE()
      CALL docXMLT.load(archtmp)
      LET root = docXMLT.getFirstDocumentNode()
      CALL docXMLT.declareNamespace(root,'cex', 'http://www.sat.gob.gt/face2/ComplementoExportaciones/0.1.0')
      CALL docXMLT.declareNamespace(root,'cfe', 'http://www.sat.gob.gt/face2/ComplementoFacturaEspecial/0.1.0')
      CALL docXMLT.declareNamespace(root,'ds', 'http://www.w3.org/2000/09/xmldsig#')
      CALL docXMLT.declareNamespace(root,'cno', 'http://www.sat.gob.gt/face2/ComplementoReferenciaNota/0.1.0')
      CALL docXMLT.declareNamespace(root,'cfc', 'http://www.sat.gob.gt/dte/fel/CompCambiaria/0.1.0')
      --DISPLAY root.toString()
      CALL docXMLT.save(fel.dir_arch_org)
      CALL fel_archivo_elimina(archtmp)
      
      LET fel.estatus = 4 --GENERÓ EL XML EN ARCHIVO ORIGINAL
      
      --LET comando = '/opt/csw/java/jdk/jdk8/bin/java -jar FirmaDocumentos-1.0.jar ', 
      LET comando = 'java -jar FirmaDocumentos-1.0.jar ', 
                    fel.dir_arch_org CLIPPED,' ', 
                    conexion.llave_archivo CLIPPED,' ',
                    conexion.llave_pass CLIPPED,' ', 
                    fel.dir_arch_fir CLIPPED
      DISPLAY comando 
      RUN comando

      LET fel.estatus = 5 --GENERÓ EL XML FIRMADO

      LET resultado = TRUE
      LET docXML = xml.DomDocument.CREATE()
      CALL docXML.load(fel.dir_arch_fir)
      LET fel.xml_docto = docXML.saveToString()
      LET fel.estatus = 6 -- LEVANTÓ EL ARCHIVO FIRMADO A MEMORIA
  CATCH
    LET mensaje = "FEL 2.0 (Archivo) ERROR :",STATUS||" ("||SQLCA.SQLERRM||")"
    LET resultado = FALSE
    LET fel.flag_error = 1
    LET fel.msg_error = mensaje
  END TRY

   LET comando = "chmod 777 ", fel.dir_arch_org
   DISPLAY "Comando ", comando 
   RUN comando 
   LET comando = "chmod 777 ", fel.dir_arch_fir
   DISPLAY "Comando ", comando 
   RUN comando 
   LET comando = "chmod 777 ", fel.dir_arch_cer
   DISPLAY "Comando ", comando  
   RUN comando
  
END FUNCTION
