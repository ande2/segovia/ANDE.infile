IMPORT XML

SCHEMA segovia

GLOBALS "fel_globales.4gl"
GLOBALS "../Library/ws_g4s/ws_g4s_fel.inc"
GLOBALS "../Library/sat/sat_xml_docto.inc"
GLOBALS "../Library/sat/sat_xml_anula.inc"
GLOBALS "../Library/sat/sat_xml_fcam.inc"

FUNCTION fel_adenada_nota()
DEFINE IdInterno  STRING
DEFINE numero      STRING
DEFINE i, j, a     SMALLINT
DEFINE wrtAdenda  xml.StaxWriter
DEFINE docAdenda  xml.DomDocument
DEFINE archivo    STRING
DEFINE valor      STRING

    CALL STARTLOG("ERR_ANDE")
    
    LET numero = factura.num_doc
    LET IdInterno = 'Ref. ', factura.tipod CLIPPED, ' ', factura.serie CLIPPED, '-', numero.trimRight()
    LET j = detcods.getLength()

    LET a = 1
    LET ns1GTDocumento.SAT.Adenda._LIST_0[a] = fel_create_docXML('IdInterno',IdInterno)
    LET a = a + 1
    LET ns1GTDocumento.SAT.Adenda._LIST_0[a] = fel_create_docXML('TotalEnLetras',factura.total_en_letras)
    LET a = a + 1
    LET valor = factura.tasa1
    LET ns1GTDocumento.SAT.Adenda._LIST_0[a] = fel_create_docXML('TasaDeCambio', valor.trimRight())
    LET a = a + 1
    LET ns1GTDocumento.SAT.Adenda._LIST_0[a] = fel_create_docXML('CuentaNo','000')

    IF j >= 0 THEN
        LET archivo = fel_archivo_genera()
        
        LET wrtAdenda = xml.StaxWriter.Create()
        CALL wrtAdenda.setFeature("format-pretty-print",TRUE)
        CALL wrtAdenda.writeTo(archivo)
        CALL wrtAdenda.startDocument("utf-8","1.0",FALSE)
            
            CALL wrtAdenda.startElement("Detalles")
            FOR i = 1 TO j
                CALL wrtAdenda.startElement("Detalle")
                    CALL wrtAdenda.startElement("Codigo")
                    CALL wrtAdenda.characters(detcods[i].codigo CLIPPED)
                    CALL wrtAdenda.endElement()
                CALL wrtAdenda.endElement()
            END FOR
            CALL wrtAdenda.endElement()
            
        CALL wrtAdenda.endDocument()
        CALL wrtAdenda.CLOSE()

        LET docAdenda = xml.DomDocument.Create()
        CALL docAdenda.setFeature("whitespace-in-element-content",FALSE)

        CALL docAdenda.LOAD(archivo)
        CALL fel_archivo_elimina(archivo)

        LET a = a + 1
        LET ns1GTDocumento.SAT.Adenda._LIST_0[a] = docAdenda

    END IF 

END FUNCTION

FUNCTION fel_complento_nota()
DEFINE wrtNota  xml.StaxWriter
DEFINE docNota  xml.DomDocument
DEFINE fecha        STRING
DEFINE docto        RECORD LIKE facturafel_e.*
DEFINE archivo   STRING
DEFINE numero,
       numero2    STRING

    CALL STARTLOG("ERR_ANDE")

    LET archivo = fel_archivo_genera()
    
    
    IF factura.estado_doc CLIPPED = 'ANTIGUO' THEN
        LET fecha = YEAR(factura.ant_fecemi) USING "&&&&", '-',
                    MONTH(factura.ant_fecemi) USING "&&", '-',
                    DAY(factura.ant_fecemi) USING "&&"
        LET numero = factura.ant_numdoc USING '####################'
        LET numero2 = numero.trimLeft()
        LET wrtNota = xml.StaxWriter.Create()
        CALL wrtNota.setFeature("format-pretty-print",TRUE)
        CALL wrtNota.writeTo(archivo)
        CALL wrtNota.startDocument("utf-8","1.0",FALSE)

        CALL wrtNota.setPrefix('cno','http://www.sat.gob.gt/face2/ComplementoReferenciaNota/0.1.0')
        CALL wrtNota.emptyElementNS("ReferenciasNota",'http://www.sat.gob.gt/face2/ComplementoReferenciaNota/0.1.0')
        CALL wrtNota.ATTRIBUTE('Version','0')
        CALL wrtNota.ATTRIBUTE('RegimenAntiguo','Antiguo')
        CALL wrtNota.ATTRIBUTE('NumeroAutorizacionDocumentoOrigen',factura.ant_resoluc CLIPPED)
        CALL wrtNota.ATTRIBUTE('SerieDocumentoOrigen', factura.ant_serie CLIPPED)
        CALL wrtNota.ATTRIBUTE('NumeroDocumentoOrigen',numero2 CLIPPED)
        CALL wrtNota.ATTRIBUTE('FechaEmisionDocumentoOrigen',fecha)
        CALL wrtNota.ATTRIBUTE('MotivoAjuste','Ajuste de precios.')

        
        CALL wrtNota.endDocument()
        CALL wrtNota.CLOSE()
    ELSE
        SELECT * INTO docto.*
          FROM facturafel_e
         WHERE fac_id = factura.id_factura
         
        LET fecha = docto.fecha USING 'YYYY-MM-DD'
    
        LET wrtNota = xml.StaxWriter.Create()
        CALL wrtNota.setFeature("format-pretty-print",TRUE)
        CALL wrtNota.writeTo(archivo)
        CALL wrtNota.startDocument("utf-8","1.0",FALSE)

        CALL wrtNota.setPrefix('cno','http://www.sat.gob.gt/face2/ComplementoReferenciaNota/0.1.0')
        CALL wrtNota.emptyElementNS("ReferenciasNota",'http://www.sat.gob.gt/face2/ComplementoReferenciaNota/0.1.0')
        CALL wrtNota.ATTRIBUTE('Version','0')
        CALL wrtNota.ATTRIBUTE('NumeroAutorizacionDocumentoOrigen',docto.autorizacion CLIPPED)
        CALL wrtNota.ATTRIBUTE('SerieDocumentoOrigen', docto.serie_e CLIPPED)
        CALL wrtNota.ATTRIBUTE('NumeroDocumentoOrigen',docto.numdoc_e CLIPPED)
        CALL wrtNota.ATTRIBUTE('FechaEmisionDocumentoOrigen',fecha)
        CALL wrtNota.ATTRIBUTE('MotivoAjuste','Ajuste de precios.')

        
        CALL wrtNota.endDocument()
        CALL wrtNota.CLOSE()
    END IF
    LET docNota = xml.DomDocument.Create()
    CALL docNota.setFeature("whitespace-in-element-content",FALSE)

    CALL docNota.LOAD(archivo)

    CALL fel_archivo_elimina(archivo)

RETURN docNota
END FUNCTION