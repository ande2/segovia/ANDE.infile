IMPORT XML

SCHEMA segovia

GLOBALS "fel_globales.4gl"
GLOBALS "../Library/ws_g4s/ws_g4s_fel.inc"
GLOBALS "../Library/sat/sat_xml_docto.inc"
GLOBALS "../Library/sat/sat_xml_anula.inc"
GLOBALS "../Library/sat/sat_xml_fcam.inc"

FUNCTION fel_adenada_fact()
DEFINE IdInterno  STRING
DEFINE numero      STRING
DEFINE i, j, a     SMALLINT
DEFINE wrtAdenda  xml.StaxWriter
DEFINE docAdenda  xml.DomDocument
DEFINE archivo    STRING
DEFINE valor      STRING
--DEFINE lpedido    LIKE factura.pedido
DEFINE loperador  LIKE fac_mtransac.usrope
DEFINE lclase     STRING --LIKE cte_ped.clase
DEFINE fec1, fec2 DATE 

    CALL STARTLOG("ERR_ANDE")
    
    LET numero = factura.num_doc
    LET IdInterno = 'Ref. ', factura.tipod CLIPPED, ' ', factura.serie CLIPPED, '-', numero.trimRight()
    LET j = detcods.getLength()

    LET a = 1
    LET ns1GTDocumento.SAT.Adenda._LIST_0[a] = fel_create_docXML('IdInterno',IdInterno)
    LET a = a + 1
    LET ns1GTDocumento.SAT.Adenda._LIST_0[a] = fel_create_docXML('TotalEnLetras',factura.total_en_letras)
    LET a = a + 1
    LET valor = factura.tipo_cambio
    LET ns1GTDocumento.SAT.Adenda._LIST_0[a] = fel_create_docXML('TasaDeCambio', valor.trimRight())

   --Cuenta
    LET a = a + 1
    --SELECT NVL(cliente, '000'), pedido INTO valor, lpedido FROM factura f WHERE f.serie = factura.serie AND f.docmto = factura.num_doc AND tipofc = 'F'
    LET valor = '0'
    LET ns1GTDocumento.SAT.Adenda._LIST_0[a] = fel_create_docXML('CuentaNo',valor)

    --Credito
    LET a = a + 1
    LET ns1GTDocumento.SAT.Adenda._LIST_0[a] = fel_create_docXML('Credito','X')
    
   --Vendedor
    LET a = a + 1
    {SELECT NVL(vendedor, '0'), NVL(usuario, '0'), NVL(clase, '0'), fe_del, fe_al
      INTO valor, loperador, lclase, fec1, fec2
      FROM cte_ped c WHERE c.bodega = 1 AND c.docmto = lpedido }
    SELECT usrope INTO loperador FROM fac_mtransac WHERE lnktra = factura.num_int 
    LET valor = '0'
    LET ns1GTDocumento.SAT.Adenda._LIST_0[a] = fel_create_docXML('CuentaNo', valor)
    
    --Operador
    LET a = a + 1
    LET valor = loperador 
    LET ns1GTDocumento.SAT.Adenda._LIST_0[a] = fel_create_docXML('Operador', valor)

    --Tipo Venta
    LET a = a + 1
    CASE 
      WHEN lclase = 'R' LET valor = 'REPOSICION'
      WHEN lclase = 'E' LET valor = 'REBAJA'
      WHEN lclase = 'P' LET valor = 'PEDIDO'
      WHEN lclase = 'L' LET valor = 'LIQUIDACION'
      WHEN lclase = 'X' LET valor = 'REPOSICION X'
      OTHERWISE LET valor = '-'
    END CASE
    LET ns1GTDocumento.SAT.Adenda._LIST_0[a] = fel_create_docXML('TipoVenta',valor)
    
    LET a = a + 1
    LET ns1GTDocumento.SAT.Adenda._LIST_0[a] = fel_create_docXML('OrdenCompra','0')
    LET a = a + 1

   --Fechas de venta
    IF fec1 IS NOT NULL AND fec2 IS NOT NULL THEN 
      LET valor = fec1, ' al ', fec2
    ELSE
      LET valor = '0'
    END IF
    LET ns1GTDocumento.SAT.Adenda._LIST_0[a] = fel_create_docXML('VentasCorrespondientes',valor)
    
    {LET a = a + 1
    LET valor = '                              DERECHOS QUE ESTE TITULO INCORPORA\n', 
                'EL ACEPTANTE COMPRADOR DEBERA CANCELAR LA TOTALIDAD DE SU IMPORTE, QUE ASCIENTE A LA SUMA DE: ______________________ ',
                'Q.______________ SIN PROTESTO Y A LA ORDEN O ENDOSO DE "PERCO, S.A."; SIN COBRO NI REQUERIMIENTO EN SUS OFICINAS DE LA ',
                'CIUDAD DE GUATEMALA, EL __ DE _____________ DE _______. LOS INTERESES MORATORIOS SON A RAZON DEL ___% MENSUAL EL ',
                'ACEPTANTE-COMPRADOR RECIBE A SU SATISFACCION LA MERCADERIA, EN LAS FECHAS INDICADAS\n',
                '\n',
                '___________________________________________     _____________________________________________  4a. CALLE 20-15, ZONA 14 BODEGA #5\n',
                'NOMBRE O RAZON SOCIAL DEL LIBRADO COMPRADOR     NOMBRE O RAZON SOCIAL DEL ACEPTANTE-COMPRADOR      DIRECCION\n',
                'A._________________________________________     _____________________________________________ __________________________________\n',
                '     DIRECCION                                     FIRMA DEL ACEPTANTE O SU REPRESENTATE             LUGAR Y FECHA\n',
                'A._________________________________________     _____________________________________________ ____________________________________\n',
                '     LUGAR Y FECHA DE ACEPTACION                           PERCO, S.A.                         NOMBRE O RAZON SOCIAL DEL AVALISTA\n',
                '                                                                                              ___________________________________\n',
                '                                                                                                  FIRMA DEL AVALISTA'
                
    LET ns1GTDocumento.SAT.Adenda._LIST_0[a] = fel_create_docXML('TextoFacturaCambiaria',valor)}

    IF j >= 0 THEN
        LET archivo = fel_archivo_genera()
        
        LET wrtAdenda = xml.StaxWriter.Create()
        CALL wrtAdenda.setFeature("format-pretty-print",TRUE)
        CALL wrtAdenda.writeTo(archivo)
        CALL wrtAdenda.startDocument("utf-8","1.0",FALSE)
            
        CALL wrtAdenda.startElement("Detalles")
        FOR i = 1 TO j
            CALL wrtAdenda.startElement("Detalle")
            CALL wrtAdenda.startElement("Codigo")
            CALL wrtAdenda.characters(detcods[i].codigo CLIPPED)
            CALL wrtAdenda.endElement()
            CALL wrtAdenda.endElement()
        END FOR
        CALL wrtAdenda.endElement()
            
        CALL wrtAdenda.endDocument()
        CALL wrtAdenda.CLOSE()

        LET docAdenda = xml.DomDocument.Create()
        CALL docAdenda.setFeature("whitespace-in-element-content",FALSE)

        CALL docAdenda.LOAD(archivo)
        CALL fel_archivo_elimina(archivo)

        LET a = a + 1
        LET ns1GTDocumento.SAT.Adenda._LIST_0[a] = docAdenda

    END IF 
END FUNCTION

PRIVATE FUNCTION fel_adenda_fel(doc)
DEFINE doc      xml.DomNode
DEFINE lista    xml.DomNodeList
DEFINE nodo     xml.DomNode
DEFINE txtNodo  xml.DomNode
DEFINE nombre   STRING
DEFINE cntLista SMALLINT
DEFINE i, j, k        SMALLINT
DEFINE dato     STRING

    LET nombre = 'AdendaPerco'
    LET lista = doc.getElementsByTagName(nombre)
    LET cntLista = lista.getCount()
    IF cntLista > 0 THEN 
        FOR i = 1 TO cntLista 
            LET nodo = lista.getItem(1)
            LET j = nodo.getChildrenCount()
            FOR k = 1 TO j
                LET txtNodo = nodo.getChildNodeItem(k)
                IF k = 1 THEN
                    LET dato = txtNodo.toString()
                ELSE
                    LET dato = dato, txtNodo.toString()
                END IF
            END FOR
        END FOR
    END IF
    
RETURN dato
END FUNCTION


FUNCTION fel_complento_FACT_EXP()
DEFINE wrtFCambiaria  xml.StaxWriter
DEFINE docFCambiaria  xml.DomDocument
DEFINE archivo    STRING
DEFINE total      STRING
DEFINE total_neto STRING

    LET total_neto = factura.total_neto USING "#################&.&&####"
    LET total = total_neto.trim()
    
    CALL STARTLOG("ERR_ANDE")

    LET archivo = fel_archivo_genera()
    
    LET wrtFCambiaria = xml.StaxWriter.Create()
    CALL wrtFCambiaria.setFeature("format-pretty-print",TRUE)
    CALL wrtFCambiaria.writeTo(archivo)
    CALL wrtFCambiaria.startDocument("utf-8","1.0",FALSE)

    CALL wrtFCambiaria.setPrefix('cex','http://www.sat.gob.gt/face2/ComplementoExportaciones/0.1.0')
    
    --CALL wrtFCambiaria.startElementNS("AbonosFacturaCambiaria",'http://www.sat.gob.gt/face2/ComplementoExportaciones/0.1.0')
      --CALL wrtFCambiaria.ATTRIBUTE('Version','1')
    
      CALL wrtFCambiaria.startElementNS('Exportacion','http://www.sat.gob.gt/face2/ComplementoExportaciones/0.1.0')
         CALL wrtFCambiaria.declareDefaultNamespace('http://www.sat.gob.gt/face2/ComplementoExportaciones/0.1.0')
         --CALL wrtFCambiaria.   .declareNamespace('xsi','http://www.sat.gob.gt/face2/ComplementoExportaciones/0.1.0')
         --CALL wrtFCambiaria.attributeNS('xmlns:cex','http://www.sat.gob.gt/face2/ComplementoExportaciones/0.1.0')
         CALL wrtFCambiaria.ATTRIBUTE('Version','1')
         CALL wrtFCambiaria.declareNamespace('xsi','http://www.sat.gob.gt/face2/ComplementoExportaciones/0.1.0')
         
         CALL wrtFCambiaria.startElementNS('NombreConsignatarioODestinatario','http://www.sat.gob.gt/face2/ComplementoExportaciones/0.1.0')
            CALL wrtFCambiaria.characters(factura.nombre_r)
         CALL wrtFCambiaria.endElement()
    
         CALL wrtFCambiaria.startElementNS('DireccionConsignatarioODestinatario','http://www.sat.gob.gt/face2/ComplementoExportaciones/0.1.0')
            CALL wrtFCambiaria.characters(factura.direccion_r)
            --CALL wrtFCambiaria.characters(factura.fecha USING "YYYY-MM-DD")
         CALL wrtFCambiaria.endElement()
    
         CALL wrtFCambiaria.startElementNS('CodigoConsignatarioODestinatario','http://www.sat.gob.gt/face2/ComplementoExportaciones/0.1.0')
            CALL wrtFCambiaria.characters(factura.nit_r)
         CALL wrtFCambiaria.endElement()

         CALL wrtFCambiaria.startElementNS('NombreComprador','http://www.sat.gob.gt/face2/ComplementoExportaciones/0.1.0')
            CALL wrtFCambiaria.characters(factura.nomconcomprador)
         CALL wrtFCambiaria.endElement()

         CALL wrtFCambiaria.startElementNS('CodigoComprador','http://www.sat.gob.gt/face2/ComplementoExportaciones/0.1.0')
            CALL wrtFCambiaria.characters("C0068528")
         CALL wrtFCambiaria.endElement()

         CALL wrtFCambiaria.startElementNS('OtraReferencia','http://www.sat.gob.gt/face2/ComplementoExportaciones/0.1.0')
            CALL wrtFCambiaria.characters(factura.otrreferencia)
         CALL wrtFCambiaria.endElement()

         CALL wrtFCambiaria.startElementNS('INCOTERM','http://www.sat.gob.gt/face2/ComplementoExportaciones/0.1.0')
            CALL wrtFCambiaria.characters(factura.incoterm)
         CALL wrtFCambiaria.endElement()

         CALL wrtFCambiaria.startElementNS('NombreExportador','http://www.sat.gob.gt/face2/ComplementoExportaciones/0.1.0')
            CALL wrtFCambiaria.characters(factura.nombre_e)
         CALL wrtFCambiaria.endElement()

         CALL wrtFCambiaria.startElementNS('CodigoExportador','http://www.sat.gob.gt/face2/ComplementoExportaciones/0.1.0')
            CALL wrtFCambiaria.characters("0001")
         CALL wrtFCambiaria.endElement()
         
      CALL wrtFCambiaria.endElement()
      
    --CALL wrtFCambiaria.endElement()
    
    CALL wrtFCambiaria.endDocument()
    CALL wrtFCambiaria.CLOSE()

    LET docFCambiaria = xml.DomDocument.Create()
    CALL docFCambiaria.setFeature("whitespace-in-element-content",FALSE)

    CALL docFCambiaria.LOAD(archivo)

    CALL fel_archivo_elimina(archivo)
      
RETURN docFCambiaria
END FUNCTION