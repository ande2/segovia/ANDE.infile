#+
#+ Generated from ws_restCliLib320
#+
IMPORT com
IMPORT xml
IMPORT util
IMPORT os

#+
#+ Global Endpoint user-defined type definition
#+
TYPE tGlobalEndpointType RECORD # Rest Endpoint
    Address RECORD # Address
        Uri STRING # URI
    END RECORD,
    Binding RECORD # Binding
        Version STRING, # HTTP Version (1.0 or 1.1)
        ConnectionTimeout INTEGER, # Connection timeout
        ReadWriteTimeout INTEGER, # Read write timeout
        CompressRequest STRING # Compression (gzip or deflate)
    END RECORD
END RECORD

PUBLIC DEFINE Endpoint
    tGlobalEndpointType
    = (Address:(Uri: "https://certificador.feel.com.gt"))

# Error codes
PUBLIC CONSTANT C_SUCCESS = 0
PUBLIC CONSTANT C_NOT_FOUND = 1001
PUBLIC CONSTANT C_INTERNAL_SERVER_ERROR = 1002
PUBLIC CONSTANT C_BAD_REQUEST = 1003
PUBLIC CONSTANT C_BAD_INFO = 1004


# generated insertCountryRequestBodyType
PUBLIC TYPE insertCountryRequestBodyType RECORD
    nit_emisor    STRING,
    correo_copia  STRING,
    xml_dte       STRING 
END RECORD

# generated insertCountryResponseBodyType
PUBLIC TYPE insertCountryResponseBodyType RECORD
   resultado         BOOLEAN,
   fecha             STRING,
   origen            STRING,
   descripcion       STRING,
   control_emision  RECORD
      saldo          STRING,
      creditos       STRING
   END RECORD,
   alertas_infile    STRING,
   descripcion_alertas_infile DYNAMIC ARRAY OF STRING,
   alertas_sat       STRING,
   descripcion_alertas_sat DYNAMIC ARRAY OF STRING,
   cantidad_errores  STRING,
   descripcion_errores DYNAMIC ARRAY OF RECORD
      resultado      STRING,
      fuente         STRING,
      categoria      STRING,
      numeral        STRING,
      validacion     STRING,
      mensaje_error  STRING
   END RECORD,
   informacion_adicional   STRING,
   uuid              STRING,
   serie             STRING,
   numero            STRING,
   xml_certificado   STRING 
END RECORD

#
# VERB: POST
# DESCRIPTION :Post test
#
PUBLIC FUNCTION insertCountry(
    p_body insertCountryRequestBodyType, id_factura STRING, mopcion STRING )
    RETURNS(INTEGER, insertCountryResponseBodyType)
    DEFINE fullpath base.StringBuffer
    DEFINE contentType STRING
    DEFINE req com.HTTPRequest
    DEFINE resp com.HTTPResponse
    DEFINE resp_body insertCountryResponseBodyType
    DEFINE json_body STRING

    TRY

        # Prepare request path
        LET fullpath = base.StringBuffer.Create()
        IF mopcion = "R" THEN
            CALL fullpath.append("/fel/certificacion/v2/dte")
        ELSE 
            CALL fullpath.append("/fel/anulacion/v2/dte")
        END IF 

        # Create request and configure it
        LET req = com.HTTPRequest.Create(
                SFMT("%1%2", Endpoint.Address.Uri, fullpath.toString()))
        IF Endpoint.Binding.Version IS NOT NULL THEN
            CALL req.setVersion(Endpoint.Binding.Version)
        END IF
        IF Endpoint.Binding.ConnectionTimeout <> 0 THEN
            CALL req.setConnectionTimeout(Endpoint.Binding.ConnectionTimeout)
        END IF
        IF Endpoint.Binding.ReadWriteTimeout <> 0 THEN
            CALL req.setTimeout(Endpoint.Binding.ReadWriteTimeout)
        END IF
        IF Endpoint.Binding.CompressRequest IS NOT NULL THEN
            CALL req.setHeader(
                "Content-Encoding", Endpoint.Binding.CompressRequest)
        END IF

        # Perform request
        --REST - Metodo
        CALL req.setMethod("POST")

        --Definición de headers
        CALL req.setHeader("usuario","SEGOVIA") --alias
        CALL req.setHeader("llave","88062145D386690F898526822CAE4814") --llave
        CALL req.setHeader("identificador",id_factura) --identificador
        
        CALL req.setHeader("Accept", "application/json")
        # Perform JSON request
        CALL req.setHeader("Content-Type", "application/json")
        LET json_body = util.JSON.stringify(p_body)
        CALL req.DoTextRequest(json_body)

        # Retrieve response
        LET resp = req.getResponse()
        DISPLAY "Resultado de ejecución |200 Error | 201 Exito | 400 Bad Request | 500 Internal server error ", resp
        # Process response
        INITIALIZE resp_body TO NULL
        LET contentType = resp.getHeader("Content-Type")
        CASE resp.getStatusCode()

            WHEN 200 #Error de información enviada
                IF contentType MATCHES "*application/json*" THEN
                    # Parse JSON response                    
                    LET json_body = resp.getTextResponse()
                    CALL util.JSON.parse(json_body, resp_body)
                    --DISPLAY "entre", resp_body.resultado
                    RETURN resp.getStatusCode(), resp_body.*
                END IF
                --DISPLAY "resp_body ", resp_body.*
                RETURN resp.getStatusCode(), resp_body.*

            WHEN 201 #Created
                IF contentType MATCHES "*application/json*" THEN
                    # Parse JSON response
                    LET json_body = resp.getTextResponse()
                    CALL util.JSON.parse(json_body, resp_body)
                    RETURN C_SUCCESS, resp_body.*
                END IF
                RETURN -1, resp_body.*

            WHEN 400 #Bad Request
                RETURN C_BAD_REQUEST, resp_body.*

            WHEN 500 #Internal Server Error
                RETURN C_INTERNAL_SERVER_ERROR, resp_body.*
            
           OTHERWISE
                RETURN resp.getStatusCode(), resp_body.*     
        END CASE
    CATCH
        RETURN -1, resp_body.*
    END TRY
END FUNCTION
################################################################################

