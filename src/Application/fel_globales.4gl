SCHEMA segovia

GLOBALS

    DEFINE id INTEGER
    DEFINE fac_database STRING
    DEFINE fac_corr SMALLINT
    DEFINE fac_pdf  SMALLINT
    DEFINE intentos SMALLINT
    DEFINE pStatus  INTEGER
    DEFINE fac_cant_det SMALLINT

    #CS DEFINE factura  RECORD LIKE facturafel_e.*
	#CS Se definieron los campos del registro en forma explicita
    DEFINE factura  RECORD 
        serie char(20),
        num_doc decimal(15,0),
        tipod char(2),
        fecha DATE,
        fecha_em char(35),
        fecha_anul char(35),
        num_aut char(20),
        fecha_re char(15),
        rango_i decimal(12,0),
        rango_f decimal(12,0),
        from char(50),
        to char(50),
        cc char(50),
        formats char(10),
        tipo_doc char(12),
        estado_doc char(20),
        ant_serie varchar(20),
        ant_numdoc decimal(15,0),
        ant_fecemi DATE,
        ant_resoluc varchar(30),
        c_moneda char(3),
        tipo_cambio char(10),
        info_reg char(15),
        num_int decimal(20,0),
        nit_e char(15),
        nombre_c char(70),
        idioma_e char(2),
        nombre_e char(70),
        codigo_e char(3),
        dispositivo_e char(3),
        direccion_e char(70),
        departamento_e char(20),
        municipio_e char(20),
        pais_e char(2),
        codpos_e decimal(5,0),
        nit_r char(15),
        es_cui SMALLINT,
        nombre_r char(70),
        idioma_r char(2),
        direccion_r char(70),
        departamento_r char(20),
        municipio_r char(20),
        pais_r char(2),
        codpos_r decimal(5,0),
        total_b decimal(10,2),
        total_d decimal(10,2),
        monto_d decimal(10,2),
        total_i decimal(10,2),
        ing_netosg decimal(10,2),
        total_iva1 decimal(10,2),
        tipo1 char(10),
        base1 decimal(10,2),
        tasa1 decimal(10,2),
        monto1 decimal(10,2),
        total_neto      DECIMAL(10,2),
        serie_e         CHAR(20),
        numdoc_e        CHAR(40),
        autorizacion    CHAR(200),
        estatus         CHAR(2),
        fac_id          INT ,
        p_descuento     DECIMAL(5,2),
        --mto_descuento decimal(14,2),
        fel_msg         INTEGER,
        id_factura      INTEGER,
        total_en_letras CHAR(250),
        tipo_pago       CHAR(2),
        es_exportacion  SMALLINT,
        incoterm        CHAR(3),
        nomconcomprador VARCHAR(100),
        dirconcomprador VARCHAR(100),
        otrreferencia   VARCHAR(32),
        es_exenta       SMALLINT,
        sat_establecimiento SMALLINT 
    END RECORD 
    DEFINE detalle  RECORD LIKE facturafel_ed.*
    DEFINE conexion RECORD LIKE empresas1.*
    DEFINE fel      RECORD LIKE factura_log.*
    
    DEFINE archivo_firmado STRING
    DEFINE directorio      STRING
    DEFINE directorio2     STRING

    DEFINE detcods DYNAMIC ARRAY OF RECORD
               codigo LIKE facturafel_ed.codigo_p
           END RECORD

    DEFINE ande_desa SMALLINT 
    DEFINE ande_debug SMALLINT
END GLOBALS